package net.kamradtfamily.blackbook.signup.chum;

import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

import static org.axonframework.test.matchers.Matchers.*;

public class ChumTest {

    private FixtureConfiguration<Chum> fixture;

    @Before
    public void before() {
        fixture = new AggregateTestFixture<>(Chum.class);
    }

    @Test
    public void testCreate() {
        fixture.givenNoPriorActivity()
                .when(new Chum.CreateCommand("0", "firstName", "lastName", "email@email.com"))
                .expectSuccessfulHandlerExecution()
                .expectEventsMatching(payloadsMatching(
                        exactSequenceOf(
                        matches(e -> {
                                    // cannot predict id
                            return e instanceof Chum.CreatedEvent &&
                                    "0".equals(((Chum.CreatedEvent)e).oktaId()) &&
                                    "firstName".equals(((Chum.CreatedEvent)e).firstName()) &&
                                    "lastName".equals(((Chum.CreatedEvent)e).lastName()) &&
                                    "email@email.com".equals(((Chum.CreatedEvent)e).email());

                        }),
                        andNoMore())));
    }

    @Test
    public void testUpdate() {
        String id = "0";
        fixture.given(new Chum.CreatedEvent(id, "0", "firstName", "lastName", "email"))
                .when(new Chum.UpdateCommand(id, "updatedFirstName", "updatedLastName"))
                .expectSuccessfulHandlerExecution()
                .expectEvents(new Chum.UpdatedEvent(id, "updatedFirstName", "updatedLastName"));
    }


} 
