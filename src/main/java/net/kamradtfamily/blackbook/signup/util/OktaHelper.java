package net.kamradtfamily.blackbook.signup.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.okta.jwt.JwtVerificationException;
import lombok.extern.slf4j.Slf4j;
import net.kamradtfamily.blackbook.signup.chum.ChumProjection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.util.Map;

@Component
@Slf4j
public class OktaHelper {
    private final ObjectMapper objectMapper;
    private final String groupId;
    private final String userUrl;
    private final String apiKey;
    private final JwtUtil jwtUtil;
    private final WebClient webClient;
    public OktaHelper(@Value("${okta.userUrl}") String userUrl,
                      @Value("${okta.apiKey}") String apiKey,
                      @Value("${okta.groupId}") String groupId,
                            ObjectMapper objectMapper,
                            WebClient webClient,
                            JwtUtil jwtUtil) {
        this.userUrl = userUrl;
        this.apiKey = apiKey;
        this.objectMapper = objectMapper;
        this.jwtUtil = jwtUtil;
        this.groupId = groupId;
        this.webClient = webClient;
    }
    public Mono<ChumProjection.Summary> updateOktaWithId(ChumProjection.Summary s) {
        return webClient.post()
                .uri(userUrl + "/" + s.oktaId())
                .bodyValue(objectMapper.createObjectNode()
                        .<ObjectNode>set("profile", objectMapper.createObjectNode()
                                .put("employeeNumber", s.id())))
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "SSWS " + apiKey)
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onRawStatus(status -> status >= 500, this::doRetry)
                .onRawStatus(status -> status >= 400 && status < 500, this::logError)
                .toEntity(JsonNode.class)
                .retryWhen(Retry.backoff(5, Duration.ofSeconds(1))
                        .filter(e -> e instanceof OktaRetryableException))
                .map(HttpEntity::getBody)
                .doOnError(e -> log.error("error calling okta", e))
                .doOnNext(r -> log.info("okta update returned {}", r))
                .thenReturn(s);
    }

    public Mono<String> createOktaChum(String email, String password) {
        return webClient.post()
                .uri(userUrl)
                .bodyValue(objectMapper.createObjectNode()
                        .<ObjectNode>set("profile", objectMapper.createObjectNode()
                                .put("login", email)
                                .put("email", email)
                                .put("locale", "en_US")
                                .put("firstName", "firstName")
                                .put("lastName", "lastName"))
                        .<ObjectNode>set("credentials", objectMapper.createObjectNode()
                                .set("password", objectMapper.createObjectNode()
                                        .put("value", password)))
                        .set("groupIds", objectMapper.createArrayNode()
                                .add(groupId))
                )
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "SSWS " + apiKey)
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .onRawStatus(status -> status >= 500, this::doRetry)
                .onRawStatus(status -> status >= 400 && status < 500, this::logError)
                .toEntity(JsonNode.class)
                .retryWhen(Retry.backoff(5, Duration.ofSeconds(1))
                        .filter(e -> e instanceof OktaRetryableException))
                .map(HttpEntity::getBody)
                .doOnError(e -> log.error("error calling okta", e))
                .doOnNext(r -> log.info("okta create chum returned {}", r))
                .map(json -> json.path("id").asText(""));
    }

    public String getIdFromToken(String auth) throws JwtVerificationException {
        Map<String, Object> claims = jwtUtil.claims(auth.replace("Bearer ", "").trim());
        log.info("claims {}", claims);
        return (String) claims.getOrDefault("chumId", "");
    }

    private Mono<OktaFinalException> logError(ClientResponse clientResponse) {
        return Mono.just(clientResponse)
                .doOnNext(cr -> log.error("okta returned {}", cr.statusCode()))
                .flatMap(cr -> cr.bodyToMono(String.class))
                .onErrorReturn("")
                .doOnNext(s -> log.error("body: {} ", s))
                .map(OktaFinalException::new);
    }

    private Mono<OktaRetryableException> doRetry(ClientResponse clientResponse) {
        return Mono.just(clientResponse)
                .doOnNext(cr -> log.error("okta returned {}", cr.statusCode()))
                .flatMap(cr -> cr.bodyToMono(String.class))
                .onErrorReturn("")
                .doOnNext(s -> log.error("body: {} ", s))
                .map(OktaRetryableException::new);
    }

    public static class OktaRetryableException extends Exception {
        public OktaRetryableException(String msg) {
            super(msg);
        }
    }
    public static class OktaFinalException extends Exception {
        public OktaFinalException(String msg) {
            super(msg);
        }
    }
}
