package net.kamradtfamily.blackbook.signup.util;

import org.apache.http.HttpHost;
import org.axonframework.serialization.Serializer;
import org.axonframework.serialization.json.JacksonSerializer;
import org.elasticsearch.client.RestClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class Clients {
    @Bean
    WebClient getWebClient() {
        return WebClient.builder().build();
    }
    @Bean
    public RestClient getRestClient(@Value("${elasticsearch.host}") String host,
                                    @Value("${elasticsearch.port}") int port,
                                    @Value("${elasticsearch.scheme}") String scheme) {
        return RestClient.builder(
                new HttpHost(host, port, scheme)).build();
    }

    @Bean
    public Serializer messageSerializer() {
        return JacksonSerializer.defaultSerializer();
    }}
