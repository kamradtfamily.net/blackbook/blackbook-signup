package net.kamradtfamily.blackbook.signup.util;

import com.okta.jwt.IdTokenVerifier;
import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.JwtVerifiers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Map;

@Component
public class JwtUtil {
    private final String nonce;
    private final IdTokenVerifier idTokenVerifier;

    public JwtUtil(@Value("${okta.jwtUrl}") String jwtUrl,
                   @Value("${okta.clientId}") String clientId,
                   @Value("${okta.nonce}") String nonce) {
        this.nonce = nonce;
        this.idTokenVerifier = JwtVerifiers.idTokenVerifierBuilder()
                .setIssuer(jwtUrl)
                .setClientId(clientId)
                .build();
    }

    public Map<String, Object> claims(String token) throws JwtVerificationException {
        Jwt jwt = idTokenVerifier.decode(token, nonce);
        return jwt.getClaims();
    }
}
