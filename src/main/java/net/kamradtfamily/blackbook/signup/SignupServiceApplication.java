package net.kamradtfamily.blackbook.signup;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.Properties;

@SpringBootApplication
@Slf4j
public class SignupServiceApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SignupServiceApplication.class, args);
		Properties git = new Properties();
		git.load(SignupServiceApplication.class.getResourceAsStream("/git.properties"));
		git.entrySet().stream().forEach(es -> log.info("{}: {}", es.getKey(), es.getValue()));
	}

	@Bean
	ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

}
