package net.kamradtfamily.blackbook.signup;

import lombok.extern.slf4j.Slf4j;
import net.kamradtfamily.blackbook.signup.chum.Chum;
import net.kamradtfamily.blackbook.signup.chum.ChumProjection;
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway;
import org.axonframework.extensions.reactor.queryhandling.gateway.ReactorQueryGateway;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class SignupService {
    private final ReactorCommandGateway reactiveCommandGateway;
    private final ReactorQueryGateway reactiveQueryGateway;
    public SignupService(ReactorCommandGateway reactiveCommandGateway,
                         ReactorQueryGateway reactiveQueryGateway) {
        this.reactiveCommandGateway = reactiveCommandGateway;
        this.reactiveQueryGateway = reactiveQueryGateway;
    }

    public Mono<ChumProjection.Summary> add(String firstName, String lastName, String email, String oktaId) {
        log.info("adding user {}", oktaId);
        return reactiveCommandGateway.send(new Chum.CreateCommand(oktaId, firstName, lastName, email))
                .cast(String.class)
                .flatMap(id -> reactiveQueryGateway.query(new ChumProjection.FetchSummaryQuery(id), ChumProjection.Summary.class));
    }

    public Mono<ChumProjection.Summary> update(String firstName, String lastName, String id) {
        log.info("updating info for {}", id);
        return reactiveCommandGateway.send(new Chum.UpdateCommand(id, firstName, lastName))
                .flatMap(x -> reactiveQueryGateway.query(new ChumProjection.FetchSummaryQuery(id), ChumProjection.Summary.class));
    }

    public Mono<ChumProjection.Summary> get(String id) {
        log.info("getting info for {}", id);
        return reactiveQueryGateway.query(new ChumProjection.FetchSummaryQuery(id), ChumProjection.Summary.class);
    }
}
