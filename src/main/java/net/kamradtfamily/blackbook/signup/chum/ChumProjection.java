package net.kamradtfamily.blackbook.signup.chum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@ProcessingGroup("chum-summary")
@Slf4j
public class ChumProjection {
    private final RestClient restClient;
    private final QueryUpdateEmitter queryUpdateEmitter;
    private final ObjectMapper objectMapper;
    public ChumProjection(RestClient restClient,
                          ObjectMapper objectMapper,
                          QueryUpdateEmitter queryUpdateEmitter) {
        this.queryUpdateEmitter = queryUpdateEmitter;
        this.restClient = restClient;
        this.objectMapper = objectMapper;
    }

    @EventHandler
    public void on(Chum.CreatedEvent event) throws IOException {
        Request request = new Request(
                "POST",
                "/chum/_doc/" + event.id());
        request.setEntity(new NStringEntity(
                objectMapper.writeValueAsString(event),
                ContentType.APPLICATION_JSON));
        log.info("ES request {}", request);
        restClient.performRequest(request);
    }

    @EventHandler
    public void on(Chum.UpdatedEvent event) throws IOException {
        Request request = new Request(
                "GET",
                "/chum/_doc/" + event.id());
        log.info("ES request {}", request);
        Response response = restClient.performRequest(request);
        JsonNode json = objectMapper.readTree(response.getEntity().getContent()).path("_source");
        Summary updated = objectMapper.treeToValue(json.<ObjectNode>deepCopy()
                .put("firstName", event.firstName())
                .put("lastName", event.lastName()), Summary.class);
        request = new Request(
                "POST",
                "/chum/_doc/" + event.id());
        request.setEntity(new NStringEntity(
                objectMapper.writeValueAsString(updated),
                ContentType.APPLICATION_JSON));
        log.info("ES request {}", request);
        restClient.performRequest(request);
        queryUpdateEmitter.emit(FetchSummariesQuery.class, query -> true, updated);
    }

    @QueryHandler
    public List<Summary> handle(FetchSummariesQuery query) throws IOException {
        Request request = new Request(
                "POST",
                "/chum");
        request.setEntity(new NStringEntity("{ \"query\": { \"match_all\": {} }} ",
                ContentType.APPLICATION_JSON));
        log.info("ES request {}", request);
        Response response = restClient.performRequest(request);
        JsonNode array = objectMapper.readTree(response.getEntity().getContent());
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(array.iterator(), 0), false)
                .limit(query.limit())
                .map(json -> json.path("hits"))
                .filter(json -> json.path("total").path("value").asInt(0) > 0)
                .map(json -> json.path("hits"))
                .map(json -> {
                    try {
                        return objectMapper.treeToValue(json, Summary.class);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException("json parsing exception", e);
                    }
                })
                .collect(Collectors.toList());
    }

    @QueryHandler
    public Summary handle(FetchSummaryQuery query) throws IOException {
        Request request = new Request(
                "GET",
                "/chum/_doc/" + query.id());
        log.info("ES request {}", request);
        Response response = restClient.performRequest(request);
        JsonNode json = objectMapper.readTree(response.getEntity().getContent());
        return objectMapper.treeToValue(json.path("_source"), Summary.class);
    }

    public record FetchSummariesQuery(int limit, int offset) {
    }
    public record FetchSummaryQuery(String id) {
    }
    public record Summary(String id, String firstName, String lastName, String email, String oktaId) {
    }

}
