package net.kamradtfamily.blackbook.signup.chum;

import io.axoniq.plugin.data.protection.annotation.SubjectId;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.UUID;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
public class Chum {
    @AggregateIdentifier
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String oktaId;

    public Chum() {
        // required by Axon
    }

    @CommandHandler
    public Chum(CreateCommand command) {
        apply(new CreatedEvent(UUID.randomUUID().toString(),
                command.oktaId(),
                command.firstName(),
                command.lastName(),
                command.email()));

    }

    @EventSourcingHandler
    public void on(CreatedEvent event) {
        this.id = event.id();
        this.email = event.email();
        this.firstName = event.firstName();
        this.lastName = event.lastName();
        this.oktaId = event.oktaId();
    }

    @CommandHandler
    public void handle(UpdateCommand command) {
        apply(new UpdatedEvent(command.id(),
                command.firstName(),
                command.lastName()));
    }

    @EventSourcingHandler
    public void on(UpdatedEvent event) {
        this.firstName = event.firstName();
        this.lastName = event.lastName;
    }

    public record UpdatedEvent(String id, String firstName, String lastName) {
    }
    public record UpdateCommand(@TargetAggregateIdentifier String id, String firstName, String lastName) {
    }
    public record CreatedEvent(@SubjectId String id, String oktaId, String firstName,
                               String lastName, String email) {
    }
    public record CreateCommand(String oktaId, String firstName, String lastName,
                                String email) {
    }

}
