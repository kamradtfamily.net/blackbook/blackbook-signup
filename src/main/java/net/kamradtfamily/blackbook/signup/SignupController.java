package net.kamradtfamily.blackbook.signup;

import com.fasterxml.jackson.databind.JsonNode;
import com.okta.jwt.JwtVerificationException;
import lombok.extern.slf4j.Slf4j;
import net.kamradtfamily.blackbook.signup.chum.ChumProjection;
import net.kamradtfamily.blackbook.signup.util.OktaHelper;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequestMapping("/api/signup")
public class SignupController {
    private final SignupService signupService;
    private final OktaHelper oktaHelper;
    public SignupController(SignupService signupService,
                            OktaHelper oktaHelper) {
        this.signupService = signupService;
        this.oktaHelper = oktaHelper;
    }
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Mono<ChumProjection.Summary> getChum(
            @RequestHeader("Authorization") String auth
    ) throws JwtVerificationException {
        return signupService.get(oktaHelper.getIdFromToken(auth));
    }

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Mono<ChumProjection.Summary> updateChum(
            @RequestHeader("Authorization") String auth,
            @RequestBody JsonNode body
    ) throws JwtVerificationException {
        return signupService.update(body.path("firstName").asText(""),
                body.path("lastName").asText(""),
                oktaHelper.getIdFromToken(auth));
    }
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    Mono<ChumProjection.Summary> createChum(
            @RequestBody JsonNode body
    ) {
        return oktaHelper.createOktaChum(body.path("email").asText(""),
                          body.path("password").asText(""))
                .flatMap(id -> signupService.add(body.path("firstName").asText(""),
                        body.path("lastName").asText(""),
                        body.path("email").asText(""),
                        id))
                .doOnNext(s -> log.info("created chum {}", s))
                .flatMap(oktaHelper::updateOktaWithId);
    }

}
